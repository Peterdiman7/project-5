import { Typography } from "@mui/material";
import { useState } from "react";
import { useEffect } from "react";
import styles from "../time/Time.module.scss";

export default function Time() {
  const [time, setTime] = useState(0);

  useEffect(() => {
    setInterval(() => {
      const current = new Date();
      const time = current.toLocaleTimeString("en-US", {
        hour: "2-digit",
        minute: "2-digit",
        hour12: false,
      });
      setTime(time);
    }, 1000);
  }, []);

  return (
    <div className={styles["time-wrapper"]}>
      <div className={styles.text}>
        <Typography sx={{ lineHeight: 1.5, color: "#8a5ef4" }}>
          Time
        </Typography>
        <Typography variant="h1" sx={{ lineHeight: 1.5 }}>
          {time}
        </Typography>
      </div>
    </div>
  );
}
