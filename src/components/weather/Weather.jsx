import { Typography } from "@mui/material";
import styles from "../weather/Weather.module.scss";

export default function Weather({ degrees = '', type = '' }) {
  return (
    <>
    <div className={styles["weather-wrapper"]}>
        <img src={type} alt="weather-img" className={styles.img} />
        </div>
        <div className={styles.text}>
        <Typography sx={{ lineHeight: 1.5, color: "#8a5ef4" }}>Weather</Typography>
        <Typography variant="h1"  sx={{ lineHeight: 1.5 }}>22°</Typography>
    </div>
    </>

  )
}