import { Grid, Paper } from "@mui/material";
import styles from "../header/Header.module.scss";
import Time from "../time/Time";
import User from "../user/User";
import Weather from "../weather/Weather";

export default function Header({ right = "", left = "" }) {
  return (
    <Paper className={styles["header-wrapper"]}>
      <User name="John Doe" avatar={"/images/avatar.ong"} size="60px" />
      <Weather type={"/images/snowy.svg"} />
      <Time />
    </Paper>
  );
}
