import { Button, InputAdornment, TextField } from "@mui/material";
import styles from "../login/Login.module.scss";
import EmailIcon from "@mui/icons-material/Email";
import LockIcon from "@mui/icons-material/Lock";
function Login() {
  return (
    <form className={styles["login-form"]}>
      <TextField
        className={styles.texField}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">{<EmailIcon />}</InputAdornment>
          ),
        }}
      />
      <TextField sx={{ marginTop: "10px", marginBottom: "10px" }}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">{<LockIcon />}</InputAdornment>
        ),
      }}
      />
      <Button variant="contained" sx={{ borderRadius: "24px" }}>
        Login
      </Button>
    </form>
  );
}

export default Login;
