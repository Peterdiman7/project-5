import { Avatar, Container, Grid, Typography } from "@mui/material";
import styles from "../user/User.module.scss";

export default function User({ avatar = "", name = "", size = "" }) {
  return (
    <div className={styles["user-container"]}>
      <Grid container>
        <Container>
          <Avatar sx={{ width: size, height: size }} src={avatar} />
          <Typography
            sx={{
              lineHeight: 2.7,
              marginLeft: "3px",
              fontFamily: "Helvetica",
            }}
            className={styles.name}
            >
            {name}
          </Typography>
        </Container>
      </Grid>
    </div>
  );
}
