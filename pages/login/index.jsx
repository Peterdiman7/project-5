import styles from "./LoginPage.module.scss";
import Login  from "../../src/components/login/Login";
import { Container, Paper } from "@mui/material";

function LoginPage() {
  return (
    <div className={styles.theme}>
    <div className={styles.wrapper}>
        <Container>
            <Paper>
                <Login />
            </Paper>
        </Container>
    </div>
    </div>
  )
}

export default LoginPage;