import Example from "../src/components/example/Example";
import Header from "../src/components/header/Header";
import Time from "../src/components/time/Time";
import User from "../src/components/user/User";
import Weather from "../src/components/weather/Weather";
import LoginPage from "./login/index.jsx";

export default function Index() {
  return <Header />;
}
